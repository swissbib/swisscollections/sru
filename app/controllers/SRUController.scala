/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package controllers

import ch.swisscollections.sru.common.validation.SruQueryParameterChecker
import ch.swisscollections.sru.runner.SruRunner

import javax.inject.Inject
import modules.{SolrJClient, SruRepository}

import play.api.Configuration
import play.api.mvc._


//todo: Unterschied in Configuration und Config??? => implicit parameter in run method
class SRUController @Inject()(cc: ControllerComponents,
                              )(implicit
                                 solrClient:SolrJClient,
                                 config: Configuration,
                                repository:SruRepository) extends AbstractController(cc) with Rendering {

  def searchRetrieve(operation: Option[String],
                     version: Option[String],
                     query:Option[String], //Option because of explain operation - not very elegant
                     startRecord: Option[Int],
                     maximumRecords:Option[Int],
                     recordPacking:Option[String],
                     recordSchema:Option[String]): Action[AnyContent] =
    Action { implicit request =>


      val qP: Map[String, Seq[String]] = request.queryString
      val runner: SruRunner = new SruQueryParameterChecker(
        operation = Option(operation.getOrElse("explain")),

        allQueryParameter = qP,
        configuration = repository.sruConfig,
        version = version,
        query = query,
        startRecord = startRecord,
        maximumRecords = maximumRecords,
        recordPacking = recordPacking,
        recordSchema = recordSchema,
        databaseEndpoint = Option.empty[String]).checkQueryParameter

      val response = runner.run().createResponse.toString()

      Ok(response).as("text/xml")


    }

  def searchRetrieveWithDatabase(operation: Option[String],
                     version: Option[String],
                     query:Option[String], //Option because of explain operation - not very elegant
                     startRecord: Option[Int],
                     maximumRecords:Option[Int],
                     recordPacking:Option[String],
                     recordSchema:Option[String],
                     dataBaseEndpoint: String): Action[AnyContent] =
    Action { implicit request =>
      val qP: Map[String, Seq[String]] = request.queryString
      val runner: SruRunner = new SruQueryParameterChecker(
        operation = Option(operation.getOrElse("explain")),

        allQueryParameter = qP,
        configuration = repository.sruConfig,
        version = version,
        query = query,
        startRecord = startRecord,
        maximumRecords = maximumRecords,
        recordPacking = recordPacking,
        recordSchema = recordSchema,
        databaseEndpoint = Option(dataBaseEndpoint)).checkQueryParameter

      val response = runner.run().createResponse.toString()

      //todo: how to return json responses?
      // is this allowed as we have done it in the old server
      Ok(response).as("text/xml")
    }


  def form(): Action[AnyContent] =
    Action { implicit request =>
      Ok("interaktive form not implemented in first version - will be done with Scala.js")
    }

  def fallback: Action[AnyContent] =
    Action { implicit request =>
      val apiBaseEndpointText = """base endpoint of sru.swisscollections.ch
          |please use endpoints for currently existing databases:
          |sru.swisscollections.ch/ao
          |sru.swisscollections.ch/swisscollections
          |""".stripMargin


     Ok(apiBaseEndpointText)
    }


}