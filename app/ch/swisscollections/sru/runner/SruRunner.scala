/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.runner

import ch.swisscollections.sru.common.validation.{CheckedExplainParameter, CheckedSearchRetrieveParameter, SystemErrorParameter}
import ch.swisscollections.sru.response.{ExplainResponse, SearchRetrieveResponse, SruResponse}
import modules.SruRepository
import play.api.Configuration

import scala.util.{Failure, Success}


abstract class SruRunner {
  def run()(implicit config: Configuration, repository: SruRepository): SruResponse
}



case class SearchRetrieveRunner(parameter: CheckedSearchRetrieveParameter) extends SruRunner {

  //todo: Es fehlt noch eine Response für no content
  override def run()(implicit config: Configuration, repository: SruRepository): SruResponse = {

    repository.searchRetrieve(
      parameter.query,
      parameter.startRecord,
      parameter.maximumRecords,
      parameter.recordSchema,
      parameter.databaseEndpointFilter) match {
        case Success(resultList: ResultList) =>

          SearchRetrieveResponse(repository.sruConfig,parameter, resultList)

        case Failure(exception) =>
          //println(exception.getMessage)
          SruRunnerFailure(SystemErrorParameter(parameter.operation,parameter.allQueryParameter,"400", exception.getMessage)).run()
    }

  }

}



case class ExplainRunner(parameter: CheckedExplainParameter) extends SruRunner {

  //todo: Es fehlt noch eine Response für no content
  override def run()(implicit config: Configuration, repository: SruRepository): SruResponse = {

    repository.explain(parameter.databaseEndpointPath) match {
      case Success(explainContent:String) =>

        ExplainResponse(repository.sruConfig,parameter, explainContent)

      case Failure(exception) =>
        //println(exception.getMessage)
        SruRunnerFailure(SystemErrorParameter(parameter.operation,parameter.allQueryParameter,"400", exception.getMessage)).run()
    }

  }

}



/*

todo: we need Runner Types for bad parameter and system Errors
case class BadArgumentsErrorRunner(parameter: BadArgumentsParameter) extends OaiRunner {

  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse =
    BadArgumentsResponse(config, repository,parameter)
}

case class SystemErrorRunner(parameter: SystemErrorParameter) extends OaiRunner {

  override def run()(implicit config: Configuration, repository: OaiRepository): OaiResponse =
    SystemErrorResponse(config, repository,parameter)
}


*/

