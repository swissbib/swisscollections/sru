package ch.swisscollections.sru.runner

import modules.SruContent

case class ResultList(nextRecordPosition:Int, numberOfRecords: Int, result:Seq[SruContent])
