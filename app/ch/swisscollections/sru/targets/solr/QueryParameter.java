package ch.swisscollections.sru.targets.solr;

import java.util.Optional;

public class QueryParameter {

    private final String _cqlQuery;
    private final Integer _startRecord;
    private final Integer _maximumRecords;
    private final Optional<String> _cursor;
    private final String _solrCollection;



    public QueryParameter(String cqlQuery, Integer startRecord,
                          Integer maximumRecords, Optional<String> cursor,
                          String solrCollection) {
        _cqlQuery = cqlQuery;
        _startRecord = startRecord;
        _maximumRecords = maximumRecords;
        _cursor = cursor;
        _solrCollection = solrCollection;


    }

    public String get_cqlQuery() {
        return _cqlQuery;
    }

    public Integer get_startRecord() {
        return _startRecord;
    }

    public Integer get_maximumRecords() {
        return _maximumRecords;
    }

    public Optional<String> get_cursor() {
        return _cursor;
    }

    public String get_solrCollection() {
        return _solrCollection;
    }
}
