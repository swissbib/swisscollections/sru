/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.targets.solr;


import java.util.ArrayList;

public class EdismaxSolrQueryClause extends BasicSolrQuery {


    protected ArrayList<String> queryFields;


    public EdismaxSolrQueryClause(String cqlIndexName, String cqlRelation, String cqlTerm ) {
        super(cqlIndexName,cqlRelation,cqlTerm);
    }


    public EdismaxSolrQueryClause(String cqlIndexName, String cqlRelation, String cqlTerm,   ArrayList<String> queryFields) {
        this(cqlIndexName,cqlRelation,cqlTerm);

        this.queryFields = queryFields;

    }


    @Override
    public String getQueryClause() {
        //for the transformation compare
        //http://www.loc.gov/standards/sru/cql/contextSets/theCqlContextSet.html


        StringBuilder clause = new StringBuilder();
        clause.append("({!edismax qf=\"");

        int numberQueryFields = 1;
        for (String field: this.queryFields) {
            if (numberQueryFields < queryFields.size()) {
                clause.append(field).append(" ");
            }
            else {
                clause.append(field);
            }

            numberQueryFields++;
        }
        clause.append("\" q.op=").append(this.mapCQLRelationToSOLRBooleanOperator()).append(" df=title_short v='").append(this.createQueryTerm()).append("'})");

            return clause.toString();
    }
}
