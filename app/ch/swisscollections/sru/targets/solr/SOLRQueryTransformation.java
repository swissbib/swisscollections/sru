/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.targets.solr;



import ch.swisscollections.sru.common.util.SruJavaConfig;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import ch.swisscollections.sru.resources.RequestedSchema;
import ch.swisscollections.sru.targets.common.BasicQueryTransformation;
import ch.swisscollections.sru.targets.common.SRUException;
import org.z3950.zing.cql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;


public class SOLRQueryTransformation extends BasicQueryTransformation {


    protected SolrDocumentList result = null;

    protected ArrayList<HashMap<String,String>> listOfResults = new ArrayList<HashMap<String, String>>();

    protected String generalFilterQuery = null;

    protected String currentSchema;

    protected int restrictedNumberOfDocuments = 50000;


    //private final static SolrServer solrServer;

    //static {
    //    solrServer = new HttpSolrServer("http://localhost:8080/solr/sb-biblio");
    //}


    public void setGeneralFilterQuery(String fq) {

        this.generalFilterQuery = fq;
    }

    public void setRestrictedNumberOfDocuments(int restrictedNumberOfDocuments) {
        this.restrictedNumberOfDocuments = restrictedNumberOfDocuments;
    }

    public void setCurrentSchema (String schema) {
        this.currentSchema = schema;
    }


    //private final String serverURL = "http://search.swissbib.ch/solr/sb-biblio/select?";
    //private final String defaultParams = "&wt=xml&indent=true";
    //http://localhost:8111/search?query=fulltext+=+%22hello%22+and+title+=+%22hello%22

    @Override
    public QueryResponse runQuery() throws Exception {

        QueryResponse response = null;

        try {

            if (null == cqlNode)  {
                throw new SRUException("no cqlNode -> target not initialized", "no cqlNode -> target not initialized");
            }


            StringBuffer luceneStringQuery = new StringBuffer();
            makeLuceneQuery(cqlNode,luceneStringQuery);


            SolrQuery parameters = new SolrQuery();
            parameters.set("q", luceneStringQuery.toString());

            this.setQueryWindow(parameters);

            if (this.generalFilterQuery != null) {

                //SOLR needs this df parameter on the local as well as on the general level
                //grr - SOLR syntax...
                parameters.set("df","title_short");
                parameters.set("fq",this.generalFilterQuery);

            }


            //seems that edismax needs a default query field
            //todo: have a closer look into the pre condition
            //parameters.set("df","title_short") ; //should be a default field if no one is defined in the configuration of the server

            //this.searchServer.setParser(new BinaryResponseParser());
            //this.searchServer.setRequestWriter(new BinaryRequestWriter());

            checkDeepPaging(parameters);

            response = this.searchServer.query(queryParams.get_solrCollection(), parameters);

        } catch (SRUException sruException) {
            throw sruException;

        } catch (Exception excep) {

            SRUException sruex = new SRUException("undefined details", "undefined message", excep);
            sruex.setUseExceptionMessage(true);
            throw sruex;

        } catch (Throwable throwable) {

            SRUException sruex = new SRUException("undefined details", "undefined message", throwable);
            sruex.setUseExceptionMessage(true);
            throw sruex;
        }

        return response;
    }

    @Override
    public SolrDocumentList getResult() {
        return result;
    }


    private void makeLuceneQuery(CQLNode node, StringBuffer sb) throws Exception{

        /*
        if (node instanceof CQLSortNode) {

            CQLSortNode sortNode = (CQLSortNode)node;
            List<ModifierSet> modifierSet = sortNode.getSortIndexes();
            CQLNode subTree =  sortNode.getSubtree();
            makeLuceneQuery(subTree, sb);
        }

        else  */
        if(node instanceof CQLBooleanNode) {

            CQLBooleanNode cbn=(CQLBooleanNode)node;
            sb.append("(");
            makeLuceneQuery(cbn.getLeftOperand(), sb);
            if(node instanceof CQLAndNode)
                sb.append(" AND ");
            else if(node instanceof CQLNotNode)
                sb.append(" NOT ");
            else if(node instanceof CQLOrNode)
                sb.append(" OR ");
            else sb.append(" UnknownBoolean(").append(cbn).append(") ");

            makeLuceneQuery(cbn.getRightOperand(), sb);
            sb.append(")");
        }
        else if(node instanceof CQLTermNode) {
            CQLTermNode ctn=(CQLTermNode)node;
            SruJavaConfig.IndexTermStructure iTS =  this.sruJavaConfig.getIndexTermStructure(ctn);

            EdismaxSolrQueryClause edismaxClause = new EdismaxSolrQueryClause(iTS.index,iTS.relation,iTS.term, iTS.solrFieldList);

            sb.append(edismaxClause.getQueryClause());
        }



    }

    private void setQueryWindow(SolrQuery parameters) {

        //todo: implement cursor later
        Optional<String> cursor = queryParams.get_cursor();

        if (cursor.isPresent())  {

            String cursorValue = cursor.get();
            //todo include special cases for sort
            if (cursorValue.equalsIgnoreCase("0") || cursorValue.equalsIgnoreCase("")) {
                parameters.set("cursorMark","*");
            } else {
                parameters.set("cursorMark",cursorValue);
            }
            try {

                parameters.set("rows", queryParams.get_maximumRecords()) ;
            } catch (Exception ex)  {

                System.out.println("invalid rows parameter -> use 10 as default");
                parameters.set("rows", 10) ;
            }
            parameters.set("sort","id asc");

            return;

        }

        //todo: implement this later
        int startRecord = queryParams.get_startRecord();

        try {
            startRecord = startRecord == 1 ? 0 : startRecord;
        } catch (Exception ex)  {

            System.out.println("invalid start parameter -> use 0 as default");
        }
        parameters.set("start",startRecord);
        //for backward compatibility - was 1 in the former version

        //todo: implement this later
        //String rows = inputParams.getFirstValue("maximumRecords");
        String rows = "10";

        try {
            int maxRows =  queryParams.get_maximumRecords() > 0 ? queryParams.get_maximumRecords() : 10;
            parameters.set("rows", maxRows) ;
        } catch (Exception ex)  {

            System.out.println("invalid rows parameter -> use 10 as default");
            parameters.set("rows", 10) ;
        }

        if (this.sruJavaConfig.getSruSchemaParameter(this.currentSchema).getSortClause().isPresent()) {
            parameters.set("sort", this.sruJavaConfig.getSruSchemaParameter(this.currentSchema).getSortClause().get());
        }

    }


    private void checkDeepPaging(SolrQuery parameters) throws SRUException{


        if (parameters.get("cursorMark") == null) {
            //deep paging using the cursor model is allowed
            //otherwise we restrict access

            int currentFetchedDocs = Integer.parseInt( parameters.get("start")) + Integer.parseInt( parameters.get("rows"));
            if (currentFetchedDocs > this.restrictedNumberOfDocuments) {
                throw new SRUException(String.format("it's not allowed to fetch more than %s documents. You fetched already %s. " +
                                "You can use the cursor model for deep paging",
                        this.restrictedNumberOfDocuments,currentFetchedDocs));
            }

        }



    }


}
