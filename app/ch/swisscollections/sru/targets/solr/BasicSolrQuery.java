/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.targets.solr;

import ch.swisscollections.sru.targets.common.SRUException;
import ch.swisscollections.sru.targets.syntaxtransformation.QueryInterface;

abstract public class BasicSolrQuery implements QueryInterface {

    //protected   CQLTermNode cqlTermNode;
    protected String cqlIndexName;
    protected String cqlRelation;
    protected String cqlTerm;

    public BasicSolrQuery(String cqlIndexName, String cqlRelation, String cqlTerm)  {

        this.cqlTerm = cqlTerm;
        this.cqlRelation = cqlRelation;
        this.cqlIndexName = cqlIndexName;

    }



    @Override
    public abstract String getQueryClause();

    protected String mapCQLRelationToSOLRBooleanOperator() {

        String solrOperator = "AND";
        //String cqlString = cqlTermNode.getRelation().toCQL();

        if (this.cqlRelation.equalsIgnoreCase("=") || cqlRelation.equalsIgnoreCase("ALL") ||
                cqlRelation.equalsIgnoreCase(">") || cqlRelation.equalsIgnoreCase("<") ||
                cqlRelation.equalsIgnoreCase(">=") || cqlRelation.equalsIgnoreCase("<=") ||
                cqlRelation.equalsIgnoreCase("NOT") || cqlRelation.equalsIgnoreCase("exact"))  {

            /*
                This is the default relation, and the server can choose any appropriate relation or means of comparing the query term
                with the terms from the data being searched. If the term is numeric, the most commonly chosen relation is '=='.
                For a string term, either 'adj' or '==' as appropriate for the index and term
             */
            solrOperator = "AND";
        } else if (this.cqlRelation.equalsIgnoreCase("ANY")) {
            solrOperator = "OR";

        } else {
            //fallback -> just for the moment
            solrOperator = "OR";
        }

        return solrOperator;

    }



    protected String createQueryTerm() {


        //String cqlString = this.cqlTermNode.getRelation().toCQL();

        StringBuilder queryTerm = new StringBuilder();



        if (this.cqlRelation.equalsIgnoreCase("exact")) {
            queryTerm.append("\"").append(this.cqlTerm).append("\"");
        } else if (this.cqlRelation.equalsIgnoreCase(">")) {

            queryTerm.append("{").append(this.cqlTerm).append(" TO *]");
        } else if (this.cqlRelation.equalsIgnoreCase("<")) {
            queryTerm.append("[* TO ").append(this.cqlTerm).append("}");
        } else if (this.cqlRelation.equalsIgnoreCase(">=")) {

            queryTerm.append("[").append(this.cqlTerm).append(" TO *]");
        } else if (this.cqlRelation.equalsIgnoreCase("<=")) {
            queryTerm.append("[* TO ").append(this.cqlTerm).append("]");
        } else if (this.cqlRelation.equalsIgnoreCase("within")) {

            String[] terms = this.cqlTerm.split(" ");
            if (terms.length == 2) {
                queryTerm.append("[").append(terms[0]).append(" TO ").append(terms[1]).append("]");
            } else if (terms.length == 1) {
                queryTerm.append("[").append(terms[0]).append(" TO ").append("*]");
            } else {
                //doesn't make really sense
                //todo: better solution later
                queryTerm.append(this.cqlTerm);
            }
        }

        else {

            queryTerm.append(this.cqlTerm);
        }

        return queryTerm.toString();

    }







}
