/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.targets.common;

import ch.swisscollections.sru.common.util.SruJavaConfig;
import ch.swisscollections.sru.targets.solr.QueryParameter;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.z3950.zing.cql.CQLNode;
import org.z3950.zing.cql.CQLParseException;
import org.z3950.zing.cql.CQLParser;

import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Pattern;


public abstract class BasicQueryTransformation implements CQLQueryTransformationInterface {



    /**
     * CQLNode to be processed by target
      */
    protected CQLNode cqlNode = null;
    protected SolrClient searchServer = null;
    protected QueryParameter queryParams = null;
    protected Map<String,ArrayList<String>> searchMapping = null;
    protected String cqlQuery = "";
    //protected UtilsCQLRelationsIndexMapping allowedRelationIndexMapping;


    protected SruJavaConfig sruJavaConfig;


    private final Pattern exactPhrase = Pattern.compile(" exact ",Pattern.CASE_INSENSITIVE);
    private final Pattern allPhrase = Pattern.compile(" all ",Pattern.CASE_INSENSITIVE);
    private final Pattern anyPhrase = Pattern.compile(" any ",Pattern.CASE_INSENSITIVE);
    private final Pattern withinPhrase = Pattern.compile(" within ",Pattern.CASE_INSENSITIVE);





    @Override
    public void init(QueryParameter queryParams,
                     SolrClient solrServer,
                     SruJavaConfig sruJavaConfig
                    ) throws Exception {

        //nur wenn icht null und length ==  0
        //setze dies in abgeleiteter Klasse für leeren String
        //this.cqlQuery = this.cqlQuery.replaceAll("\"","\\\\\"");
        //" causes errors in CQL parser -> perhaps I have to find a way to change terms with phrases into exact relation
        //this.queryParams = queryParams;

        //do we need the terms with or without double quotes??
        //we need urgently test cases for all the query mapping stuff (not part of my time at UB - GH)
        this.queryParams = queryParams;
        this.cqlQuery = queryParams.get_cqlQuery().replaceAll("\"","");
        this.sruJavaConfig = sruJavaConfig;

        if (this.cqlQuery.equals("")) {
            //todo what if empty query -> how to transform it into *:* for CQL??
            //do this in derived class because it depends on the target
            //overwrite init!
            this.cqlQuery = "dc.anywhere=*:*";
            //throw new SRUException("missing query parameter", "missing query parameter");

        }

        this.searchServer = solrServer;
        //todo logging system

        /*
        CQLParser.V1POINT2
        doesn't work with uppercase ANY or ALL
         */
        CQLParser cqlP = new CQLParser(CQLParser.V1POINT1SORT);
        //cqlP.registerCustomRelation("ALL");
        //cqlP.registerCustomRelation("ANY");
        //cqlP.registerCustomRelation("EXACT");
        this.cqlQuery = exactPhrase.matcher(this.cqlQuery).replaceAll(" exact ");
        this.cqlQuery = allPhrase.matcher(this.cqlQuery).replaceAll(" all ");
        this.cqlQuery = withinPhrase.matcher(this.cqlQuery).replaceAll(" within ");
        this.cqlQuery = anyPhrase.matcher(this.cqlQuery).replaceAll(" any ");


        try {
                cqlNode = cqlP.parse(this.cqlQuery);
        } catch (CQLParseException pE) {

            SRUException sruex = new SRUException("cql parse Exception", "cql parse Exception", pE);
            sruex.setUseExceptionMessage(true);
            throw sruex;

        } catch (Throwable throwable) {
            SRUException sruex = new SRUException("undefined details", "undefined message", throwable);
            sruex.setUseExceptionMessage(true);
            throw sruex;

        }
    }


    @Override
    public abstract QueryResponse runQuery() throws Exception;

    @Override
    public abstract SolrDocumentList getResult();


    public String getCQLQuery() {
        return this.cqlQuery;
    }


}