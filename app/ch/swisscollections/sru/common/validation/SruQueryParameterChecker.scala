/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.swisscollections.sru.common.validation

import ch.swisscollections.sru.common.util.SruJavaConfig
import ch.swisscollections.sru.common.validation.SruOperation.SruOperation
import ch.swisscollections.sru.runner.{SruRunner, SruRunnerFailure}

import scala.util.{Failure, Success}
import play.api.Configuration

case class SruCheckedOperationWithParameter(
                                operation:SruOperation,
                                version: Option[String],
                                query: Option[String],
                                startRecord: Option[Int],
                                maximumRecords: Option[Int],
                                recordPacking: Option[String],
                                recordSchema: Option[String],
                                databaseEndpoint: Option[String],
                                allQueryParameter: Map[String, Seq[String]],
                                configuration: SruJavaConfig)



class SruQueryParameterChecker(operation: Option[String],
                               version: Option[String],
                               query: Option[String],
                               startRecord: Option[Int],
                               maximumRecords: Option[Int],
                               recordPacking: Option[String],
                               recordSchema: Option[String],
                               databaseEndpoint: Option[String],
                               allQueryParameter: Map[String, Seq[String]],
                               configuration: SruJavaConfig) {
  //noinspection ScalaStyle
  def checkQueryParameter: SruRunner = {

    SruOperation.getOperation(operation) match {
          case SruOperation.SearchRetrieve =>
            SearchRetrieveValidation(
              SruCheckedOperationWithParameter(
                operation = SruOperation.SearchRetrieve,
                version = version,
                query = query,
                startRecord = startRecord,
                maximumRecords = maximumRecords,
                recordPacking = recordPacking,
                recordSchema = recordSchema,
                databaseEndpoint = databaseEndpoint,
                allQueryParameter = allQueryParameter,
                configuration = configuration
              )
            )
          case SruOperation.Explain =>
            ExplainValidation(
              SruCheckedOperationWithParameter(
                operation = SruOperation.Explain,
                version = version,
                query = query,
                startRecord = startRecord,
                maximumRecords = maximumRecords,
                recordPacking = recordPacking,
                recordSchema = recordSchema,
                databaseEndpoint = databaseEndpoint,
                allQueryParameter = allQueryParameter,
                configuration = configuration
              )
            )

          case SruOperation.WrongOperation =>
            SruRunnerFailure(SystemErrorParameter(SruOperation.WrongOperation,allQueryParameter,"500", s"""operation: ${operation.getOrElse("")} not supported"""))

          case SruOperation.MissingOperation =>
            SruRunnerFailure(SystemErrorParameter(SruOperation.MissingOperation,allQueryParameter,"500", s"""necessary parameter operation not available"""))


    }



  }
}


class MissingParameterException(message: String) extends Exception(message)
