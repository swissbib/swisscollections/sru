/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.common.validation

import ch.swisscollections.sru.common.util.SruJavaConfig
import ch.swisscollections.sru.common.validation.SruOperation.SruOperation
import play.api.Configuration

sealed abstract class SruParameterBase() extends Product with Serializable {
  val operation: SruOperation
  val allQueryParameter: Map[String, Seq[String]]
}



case class CheckedSearchRetrieveParameter (operation:SruOperation,
                                           version: String,
                                           query: String,
                                           startRecord: Int,
                                           maximumRecords: Int,
                                           recordPacking: String,
                                           recordSchema: String,
                                           databaseEndpointFilter: Option[String],
                                           allQueryParameter: Map[String, Seq[String]],
                                           configuration: SruJavaConfig) extends SruParameterBase {

  require(operation == SruOperation.SearchRetrieve)

}




case class CheckedExplainParameter (operation:SruOperation,
                                    allQueryParameter: Map[String, Seq[String]],
                                    databaseEndpointPath: String) extends SruParameterBase {

  require(operation == SruOperation.Explain)

}


case class SystemErrorParameter (operation:SruOperation
                                 ,allQueryParameter: Map[String, Seq[String]]
                                 ,errorCode: String
                                 ,errorMessage: String) extends SruParameterBase {




}


