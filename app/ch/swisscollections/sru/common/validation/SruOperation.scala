/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.common.validation

import scala.util.{Failure, Success, Try}

object SruOperation extends Enumeration {

  type SruOperation = Value

  protected case class SruVal(operation: String) extends super.Val {

    //https://www.scala-lang.org/api/current/scala/Enumeration.html
  }

  import scala.language.implicitConversions

  implicit def getOperation(x: Value): String = x.asInstanceOf[SruVal].operation

  private val internalNamesMap = Map(
    "SEARCHRETRIEVE" -> "SearchRetrieve",
    "EXPLAIN" -> "Explain",
    )


  val SearchRetrieve: SruVal = SruVal("searchRetrieve")
  val Explain: SruVal = SruVal("explain")
  val WrongOperation: SruVal = SruVal("WrongOperation")
  val MissingOperation: SruVal = SruVal("MissingOperation")

  //oh my god -- looking for real enums in Scala version 3


  private val mv = ".*None\\.get".r

  def getOperation(operation: Option[String]): SruOperation = {
      Try[SruOperation] (SruOperation.withName(
        internalNamesMap(operation.get.toUpperCase))) match {
        case Success(value) =>
          //print(value)
          value
        case Failure(exception) => if (mv.matches(exception.getMessage)) MissingOperation else WrongOperation

      }
    }




}


