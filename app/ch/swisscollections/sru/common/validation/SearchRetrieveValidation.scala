/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.common.validation

import ch.swisscollections.sru.common.validation.SruOperation.SruOperation
import ch.swisscollections.sru.runner.{SearchRetrieveRunner, SruRunner, SruRunnerFailure}

import scala.util.{Failure, Success, Try}



//noinspection ScalaStyle
object SearchRetrieveValidation extends ParameterValidationFunction {

  private val V1_1 = "1.1"
  private val V1_2 = "1.2"

  private def correctVersion(queryVersion: Option[String], operation: SruOperation): Boolean = queryVersion match {
    case Some(version) if operation == SruOperation.SearchRetrieve => version == V1_2 || version == V1_1
    case  None if operation == SruOperation.Explain => true
    case _ => false
  }

  private def versionAvailableButNotCorrect(queryVersion: Option[String]): Boolean = queryVersion match {
    case Some(version) => version != V1_2 && version != V1_1
    case _ => false
  }


  override def apply(v1: SruCheckedOperationWithParameter): SruRunner = {

    v1 match {
      case SruCheckedOperationWithParameter(
        operation,
        version,
        Some(query),
        startRecord,
        maximumRecords,
        recordPacking,
        recordSchema,
        databaseEndpoint,
        allQueryParameter,
        configuration) if (correctVersion(version, operation)) =>

        val rePa = recordPacking.map(rP => if (rP.toLowerCase != "string" && rP.toLowerCase != "xml") "xml" else rP).getOrElse("xml")
        val reSchema = recordSchema.filter(rS => configuration.existsSruSchema(rS)).getOrElse(configuration.getDefaultSruSchema).toUpperCase
        validateDatabaseEndpoint(databaseEndpoint,configuration) match {
          case Success(value) =>
            SearchRetrieveRunner(CheckedSearchRetrieveParameter(
              operation = operation,
              version = version.get,
              query = query,
              startRecord = startRecord.getOrElse(0),
              maximumRecords = maximumRecords.getOrElse(10), //todo use configuration
              recordPacking = rePa,
              recordSchema = reSchema,
              databaseEndpointFilter = value, //todo: enhance query already here??
              allQueryParameter = allQueryParameter,
              configuration = configuration
            ))
          case Failure(exception) =>
            SruRunnerFailure(SystemErrorParameter(operation,allQueryParameter,"400", s"""viewendpoint not available ${exception.getMessage}"""))
        }



      case SruCheckedOperationWithParameter(
        operation,
        version,
        Some(query),
        _,
        _,
        _,
        _,
        _,
        allQueryParameter,
        configuration) if (operation == SruOperation.SearchRetrieve
          && versionAvailableButNotCorrect(version)) =>
        SruRunnerFailure(SystemErrorParameter(operation,allQueryParameter,"400", s"""wrong version number: ${version}"""))
      case SruCheckedOperationWithParameter(
        operation,
        version,
        query,
        startRecord,
        maximumRecords,
        recordPacking,
        recordSchema,
        databaseEndpoint,
        allQueryParameter,
        configuration) =>
        SruRunnerFailure(SystemErrorParameter(operation,allQueryParameter,"400", "wrong parameters - this only as template"))
    }
  }
}




