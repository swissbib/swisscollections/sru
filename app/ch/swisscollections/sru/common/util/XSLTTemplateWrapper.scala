/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.common.util

object XSLTTemplateWrapper {

  private var templates: Option[Map[String, SruSchema]] = Option.empty

  def apply (templateName: String):SruSchema = templates.get(templateName)

  def init (initializeTemplates: Map[String,SruSchema]):Unit = {
   if (templates.isEmpty) {
     templates = Option(initializeTemplates)
   }
  }

}
