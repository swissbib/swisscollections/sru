package ch.swisscollections.sru.common.util;

import ch.swisscollections.sru.targets.common.SRUException;
import org.z3950.zing.cql.CQLRelation;
import org.z3950.zing.cql.CQLTermNode;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

public class SruJavaConfig {

    private final Map<String,String> viewEndpointSolrFilterMapping = new HashMap<>();

    private final Map<String, MappingConfigValues> mappingConfigValues = new HashMap<>();

    private final Pattern removecqlPrefix = Pattern.compile("^cql\\.");

    private Map<String,SRUSchemaParameter> sruSchemaMappings = new HashMap<>();

    private final Map<String,String> explainFrames = new HashMap<>();

    public static class SRUSchemaParameter {

        public final Boolean isDefault;
        final String sortClause;
        public SRUSchemaParameter(Boolean isDefault, String sortClause) {
            this.isDefault = isDefault;
            this.sortClause = sortClause;
        }

        public Optional<String> getSortClause () {
            if (!sortClause.equalsIgnoreCase("NONE")) {
                return Optional.of(sortClause);
            } else
                return Optional.empty();

        }
    }

    public static class MappingConfigValues {
        ArrayList<String> supportedRelations;
        String defaultRelation;
        String alternativeIndex;
        String indexName;
        ArrayList<String> solrFields;

        public MappingConfigValues (String supportedRelations,
                                    String defaultRelation,
                                    String alternativeIndex,
                                    String indexName,
                                    ArrayList<String> solrFields) {
            this.supportedRelations = new ArrayList<>(Arrays.asList(supportedRelations.split("#")));
            this.defaultRelation = defaultRelation;
            this.alternativeIndex = alternativeIndex;
            this.indexName = indexName;
            this.solrFields = solrFields;

        }

    }


    public static class IndexTermStructure {

        public String index;
        public String term;
        public String relation;
        public ArrayList<String> solrFieldList;

        public IndexTermStructure(String index, String term, String relation, ArrayList<String> solrFieldList) {
            this.index = index;
            this.term = term;
            this.relation = relation;
            this.solrFieldList = solrFieldList;

        }
    }



    public SruJavaConfig(){}


    public void addCqlRelationsMapping(MappingConfigValues mappings) {

        mappingConfigValues.put(mappings.indexName.toLowerCase(), mappings);

    }

    public void addViewPoints(Map<String,String> viewEndpointFilterMapping) {

        this.viewEndpointSolrFilterMapping.putAll(viewEndpointFilterMapping);

    }

    public void addExplainFrames(Map<String,String> explainFramesMapping) {

        this.explainFrames.putAll(explainFramesMapping);

    }


    public void addSruSchemaMapping(Map<String, SRUSchemaParameter> sruSchemaMappings) {

        this.sruSchemaMappings = sruSchemaMappings;

    }

    public boolean existsSruSchema(String schemaName) {
        return this.sruSchemaMappings.containsKey(schemaName.toUpperCase());
    }

    public String getDefaultSruSchema() {

        AtomicReference<String> schemaName = new AtomicReference<>("");
        this.sruSchemaMappings.forEach((k,v) -> {
            if (v.isDefault) {
                schemaName.set(k);
            }
        });
        return schemaName.toString();
    }


    public SRUSchemaParameter getSruSchemaParameter (String schemaName){

        return this.sruSchemaMappings.get(schemaName.toUpperCase());
    }


    public String getViewEndpoint (String endpointKey) throws SRUException{
        if (this.viewEndpointSolrFilterMapping.containsKey(endpointKey.toUpperCase()))
            return this.viewEndpointSolrFilterMapping.get(endpointKey.toUpperCase());
        else throw new SRUException("endpoint " + endpointKey + " doesn't exist");
    }

    public IndexTermStructure getIndexTermStructure(CQLTermNode cqlTermNode)  throws SRUException{


        String indexNode = removecqlPrefix.matcher(cqlTermNode.getIndex().toLowerCase()).replaceAll("");
        String term = cqlTermNode.getTerm();
        String nodeRelation = cqlTermNode.getRelation().toCQL();

        if (mappingConfigValues.containsKey(indexNode) &&
                mappingConfigValues.get(indexNode).alternativeIndex.equalsIgnoreCase("NONE")) {

            MappingConfigValues mcv =  mappingConfigValues.get(indexNode);
            //das ist ziemlich grausam, Listenbehandlung mit Java...
            AtomicReference<String> relation = new AtomicReference<>(mcv.alternativeIndex);
            mcv.supportedRelations.forEach(elem -> {
                if (elem.toLowerCase().equalsIgnoreCase(nodeRelation))
                    relation.set(nodeRelation);
            });

            return new IndexTermStructure(indexNode,term,relation.get(), mcv.solrFields);


        }
        else if (mappingConfigValues.containsKey(indexNode) &&
                !mappingConfigValues.get(indexNode).alternativeIndex.equalsIgnoreCase("NONE")) {

            //MappingConfigValues mcv = mappingConfigValues.get(indexNode);
            String alternativeIndex = mappingConfigValues.
                    get(mappingConfigValues.get(indexNode).indexName).alternativeIndex;
            MappingConfigValues definitionForParsedNode = mappingConfigValues.get(indexNode);
            /*
            if there is a configured default Relation for the node here used then this is relation is being used for the alternative index
            use case
            "motiv wetter" 0> parsed CqlNode:
            indexNode == serverChoice
            relation == scr (server choosed relation)
            because for serverChoice we define an = relation the = relation is handed over to the default index "dc.anywehre" and
            = is mapped to an and (not or)  Solr operator
            which is the desired result
             */
            String relationToUse = !definitionForParsedNode.defaultRelation.equals("") ? definitionForParsedNode.defaultRelation : nodeRelation;
            return getIndexTermStructure(new CQLTermNode(alternativeIndex,new CQLRelation(relationToUse),term));

        }
        else
            throw new SRUException("index: " + indexNode + " not supported","index: " + indexNode + " not supported");


    }

    public String getExplainFrame(String endpoint) {
        return this.explainFrames.get(endpoint.toUpperCase());
    }


}
