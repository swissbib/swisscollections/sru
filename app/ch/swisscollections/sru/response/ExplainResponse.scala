/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



package ch.swisscollections.sru.response

import ch.swisscollections.sru.common.util.SruJavaConfig
import ch.swisscollections.sru.common.validation.CheckedExplainParameter

import scala.xml.{Node, XML}

case class ExplainResponse(sruConfig: SruJavaConfig,
                           parameter: CheckedExplainParameter,
                           explainFrame: String) extends SruResponse (sruConfig, parameter) {


  override def toString: String = s"Ich bin eine ExplainResponse"


  override def createResponse: Seq[Node] = {


    XML.loadString(explainFrame)

  }




}

