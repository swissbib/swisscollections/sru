/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.response

import ch.swisscollections.sru.common.util.{SruJavaConfig, TemplateTransformer, XSLTTemplateWrapper}
import ch.swisscollections.sru.common.validation.CheckedSearchRetrieveParameter
import ch.swisscollections.sru.runner.ResultList
import modules.SruRepository
import play.api.Configuration

import java.time.Instant
import javax.xml.transform.Transformer
import scala.xml.transform.{RewriteRule, RuleTransformer}
import scala.xml.{Elem, Node, XML}

case class SearchRetrieveResponse(sruConfig: SruJavaConfig,
                                  parameter: CheckedSearchRetrieveParameter,
                                  resultList: ResultList) extends SruResponse (sruConfig, parameter)   {
  override def toString: String = s"Ich bin eine SearchRetrieveResponse"


  override def createResponse: Seq[Node] = {


    createSruFrame

    val versionAndNumbers: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case Elem(prefix,"searchRetrieveResponse", att, scope,_, child@_*) =>
          val vAn =  <version>{parameter.version}</version> ++ <numberOfRecords>{resultList.numberOfRecords}</numberOfRecords>
          Elem(prefix, "searchRetrieveResponse", att, scope,false, vAn ++ child: _*)
        case n => n
      }
    }


    /*
    val wellFormedTag : RewriteRule = new RewriteRule {
      override def transform(n: Node): collection.Seq[Node] = n match {

      }
    }

     */

    val recordsAndFooter: RewriteRule = new RewriteRule {
      override def transform(n: Node): Seq[Node] = n match {
        case records @ <records/> =>

          var recordPosition = parameter.startRecord
          val recordsList = <records>
          {
            resultList.result.map(srucontent => {

              XSLTTemplateWrapper(parameter.recordSchema.toUpperCase).xslt match {
                case Some(transformer) =>
                  val transformationResult: String = new TemplateTransformer(srucontent.fullRecord.toString()).transform(transformer)
                  val loaded = XML.loadString(transformationResult)
                  val recordData = loaded \ "recordData"
                  val recordDataExtra = loaded \ "extraRecordData"

                  <record>
                    <recordSchema>{parameter.recordSchema}</recordSchema>
                    <recordPacking>{parameter.recordPacking}</recordPacking>
                    {recordData ++
                    recordDataExtra}
                    <recordPosition>{recordPosition += 1; recordPosition}</recordPosition>
                  </record>




                case _ =>
                  
                    <record>
                      <recordSchema>{parameter.recordSchema}</recordSchema>
                      <recordPacking>{parameter.recordPacking}</recordPacking>
                      <recordData>
                    {XML.loadString(srucontent.fullRecord.toString())}
                      </recordData>
                      <recordPosition>{recordPosition += 1; recordPosition}</recordPosition>
                  </record>

              }


            })

          }
          </records>
          val allNodes = if (resultList.numberOfRecords  > (parameter.startRecord + parameter.maximumRecords)) {
            recordsList ++ <nextRecordPosition>{parameter.startRecord + parameter.maximumRecords}</nextRecordPosition>
          } else {recordsList}

          allNodes

        case _ @ <echoedSearchRetrieveRequest/> => {

          //todo: was bedeutet xQuery in diesem Kontext??
          <echoedSearchRetrieveRequest>
            <version>{parameter.version}</version>
            <query>{parameter.query}</query>
            <xQuery xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:nil="true"/>
            <recordSchema>{parameter.recordSchema}</recordSchema>
          </echoedSearchRetrieveRequest>
        }

        case n => n

      }

    }

    new RuleTransformer(recordsAndFooter).transform(new RuleTransformer(versionAndNumbers).transform(createSruFrame))

  }


}
