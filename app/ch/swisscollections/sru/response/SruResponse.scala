/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.response

import ch.swisscollections.sru.common.util.SruJavaConfig
import ch.swisscollections.sru.common.validation.SruParameterBase

import java.time.Instant
import scala.xml._


abstract class SruResponse(sruConfig: SruJavaConfig
                           ,parameter: SruParameterBase) {



  def makeHeader(del: Boolean = false,

                 datestamp: Instant = Instant.now(),
                 identifier: String): Elem = {

    //seems to be standard that in all cases a setSpec is part of the header of a record
    //if the set isn't given (optional parameter) we make a fallback to the highest set in set hierarchy
    val set = parameter.allQueryParameter.getOrElse("set",List("memobase")).head
    <header status={if (del) Some(Text("deleted")) else None}>
      <identifier>{identifier}</identifier>
      <datestamp>{datestamp}</datestamp>
      <setSpec>{set}</setSpec>
    </header>
  }


  def createRequestTag: Elem = {

    //completely scala xml voodoo...
    //https://stackoverflow.com/questions/43751677/how-to-create-xml-attributes-dynamic-via-map-in-scala
    //take some time to understand this...
    val seed: MetaData = Null
    val meta: MetaData = parameter.allQueryParameter.toList.foldLeft(seed) {
      case (acc, (s1, s2)) =>
        new UnprefixedAttribute(
          //filter out characters used for multiple parameter
          key = s1.replaceAll("""[\[\]]""", ""),
          value = s2.mkString(","),
          next = acc
        )
    }
    //first try
    //<request verb={runner.request.parameter.verb.toString}>{runner.repository.oaiConfig.identify.baseUrl}</request>
    //noinspection ScalaStyle

    //todo: this was done for OAI --> implement it for SRU
    Elem(
      prefix = null,
      label = "request",
      attributes = meta,
      scope = TopScope,
      minimizeEmpty = false,
      child = Text(sruConfig.toString)
    )
  }

  protected def createSruFrame: Elem = {
    val sruFrame = {
    //noinspection ScalaStyle


      //DNB Beispiele
      //https://www.dnb.de/DE/Professionell/Metadatendienste/Datenbezug/SRU/sru_node.html#[syntax]
      //https://services.dnb.de/sru/authorities?version=1.1&operation=searchRetrieve&query=PER%3DRupp%20Elisabeth%20and%20BBG%3DTp*&recordSchema=MARC21-xml
      /*<?xml version="1.0" encoding="UTF-8"?>*/
        <searchRetrieveResponse xmlns="http://www.loc.gov/zing/srw/">
          <records/>
          <echoedSearchRetrieveRequest/>
        </searchRetrieveResponse>

    }

    sruFrame
  }

  def createResponse:Seq[Node]

  override def toString: String = s"Ich bin eine OAI response mit Header für : $getVerb} "

  //verb should always be available
  def getVerb:String = parameter.allQueryParameter("verb").head

}














