/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


package ch.swisscollections.sru.response

import ch.swisscollections.sru.common.util.SruJavaConfig
import ch.swisscollections.sru.common.validation.{CheckedSearchRetrieveParameter, SystemErrorParameter}
import ch.swisscollections.sru.runner.ResultList

import scala.xml.Node

case class SystemErrorResponse(sruConfig: SruJavaConfig,
                               parameter: SystemErrorParameter) extends SruResponse (sruConfig, parameter) {
  override def createResponse: Seq[Node] = {

    //make it short for now
    <error>
      {parameter.errorMessage}
    </error>

  }
}
