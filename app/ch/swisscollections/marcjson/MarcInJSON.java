/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



package ch.swisscollections.marcjson;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.marc4j.marc.MarcFactory;
import org.marc4j.marc.Record;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import org.marc4j.marc.impl.Verifier;
import org.marc4j.marc.ControlField;
import org.marc4j.marc.DataField;
import org.marc4j.marc.VariableField;
import org.marc4j.marc.Subfield;


/**
 *
 * @author dueberb
 */
public class MarcInJSON {

    static private final MarcFactory factory = MarcFactory.newInstance();
    static private final ObjectMapper mapper= new ObjectMapper();
    
    static public Record new_from_hash(Map<String, Object> m) {
        Record record = factory.newRecord();

        // Set the leader
        String leaderstr = (String) m.get("leader");
        record.setLeader(factory.newLeader(leaderstr));
        ArrayList<Map> fields = (ArrayList<Map>) m.get("fields");
        for (Map f :   fields) {
            String tag = (String) f.keySet().toArray()[0];
            if (Verifier.isControlField(tag)) {
                String value = (String) f.get(tag);
                record.addVariableField(factory.newControlField(tag, value));
            } else {
                char ind1 = ' ';
                char ind2 = ' ';
                Map fdata = (Map<String, Object>) f.get(tag);
                if (fdata.containsKey("ind1")) {
                    ind1 = ((String) fdata.get("ind1")).charAt(0);
                }
                if (fdata.containsKey("ind2")) {
                    ind2 = ((String) fdata.get("ind2")).charAt(0);
                }

                DataField df = factory.newDataField(tag, ind1, ind2);
                ArrayList<Map> subfields = (ArrayList<Map>) fdata.get("subfields");

                for (Map<String, String> sub : subfields) {
                    String code = (String) sub.keySet().toArray()[0];
                    df.addSubfield(factory.newSubfield(code.charAt(0), sub.get(code)));
                }
                record.addVariableField(df);
            }
        }


        return record;

    }

    static public Record new_from_marc_in_json(String str) throws java.io.IOException {
        return new_from_hash(mapper.readValue(str, Map.class));
    }

    static public String record_to_marc_in_json(Record r) throws java.io.IOException  {
        HashMap h = record_to_hash(r);
        return mapper.writeValueAsString(h);
    }

    static public HashMap record_to_hash(Record r) {
        HashMap<String, Object> m = new HashMap<String, Object>();
        m.put("leader", r.getLeader().toString());
        ArrayList fields = new ArrayList();
        m.put("fields", fields);
        for (Object vfo : r.getVariableFields()) {
            VariableField vf = (VariableField) vfo;
            String tag = vf.getTag();
            if (Verifier.isControlField(tag)) {
                HashMap<String, String> fmap = new HashMap();
                ControlField cf = (ControlField) vf;
                fmap.put(tag, cf.getData());
                fields.add(fmap);
            } else {
                HashMap<String, HashMap> fmap = new HashMap();
                DataField df = (DataField) vf;
                HashMap<String, Object> fdata = new HashMap();
                fdata.put("ind1", String.valueOf(df.getIndicator1()));
                fdata.put("ind2", String.valueOf(df.getIndicator2()));
                ArrayList subfields = new ArrayList();
                fdata.put("subfields", subfields);
                for (Object sfo : df.getSubfields()) {
                    Subfield sf = (Subfield) sfo;
                    HashMap<String, String> sfmap = new HashMap();
                    sfmap.put(String.valueOf(sf.getCode()), sf.getData());
                    subfields.add(sfmap);
                }
                fmap.put(tag, fdata);
                fields.add(fmap);
            }

        }

        return m;
    }

}
