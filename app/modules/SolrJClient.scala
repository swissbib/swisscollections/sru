/*
 * generic Sru Server - agnostic in relation to the data repository
 * specific data repository has to implement the operations defined in SRU standard (1.2)
 * initially created for swisscollections project
 *
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package modules

import ch.swisscollections.sru.common.util.SruJavaConfig.MappingConfigValues
import ch.swisscollections.sru.common.util.{SruJavaConfig, SruSchema, TemplateCreator, XSLTTemplateWrapper}
import ch.swisscollections.sru.resources.RequestedSchema
import ch.swisscollections.sru.runner.ResultList
import ch.swisscollections.sru.targets.solr.{QueryParameter, SOLRQueryTransformation}
import com.typesafe.config.{ConfigList, ConfigObject}
import org.apache.solr.client.solrj.response.QueryResponse

import java.util
import java.util.Optional
import javax.xml.transform.Transformer
import scala.collection.mutable
import scala.language.implicitConversions
import scala.xml.XML
import com.typesafe.config.Config

import javax.inject.{Inject, Singleton}
import org.apache.solr.client.solrj.SolrClient
import org.apache.solr.client.solrj.impl.HttpSolrClient
import play.Environment
import play.api.Play
import play.api.inject.ApplicationLifecycle

import scala.concurrent.Future
import scala.io.Source
import scala.jdk.CollectionConverters._
import scala.util.Try


@Singleton
class SolrJClient @Inject()(
                              lifecycle: ApplicationLifecycle,
                              val config: Config,
                              val env: Environment
                            ) extends SolrComponent {
  lifecycle.addStopHook(() => {
    //Future.successful(client.get.close())
    Future.successful(client.getOrElse(Option.empty))
  })


  override val client: Option[SolrClient] = connect()

  override val solrCollection: String = getEnv("SOLR_COLLECTION")


  private def connect(): Option[SolrClient] = {
    //todo: make it possible to work with zookeeper

    //todo make timeouts configurable
    val client = new HttpSolrClient.Builder(getEnv("SOLR_HOST"))
      .withConnectionTimeout(6000)
      .withSocketTimeout(6000)
      .build()

    Option(client)

  }

  def getEnv(value: String): String = {
    val envValue: Option[String] = sys.env.get(value)
    if (envValue.isDefined) {
      envValue.get
    } else {
      Option(System.getProperties.get(value)) match {
        case Some(value) => value.toString
        case None => throw new Exception("Environment variable " + value + " not available")
      }
    }
  }


  override val sruConfig: SruJavaConfig = {

    val mappingViewFilter = config.getObjectList("sru.mappingviewssolr").asScala.toSeq.map {
      o => {
        val key = o.keySet.asScala.head
        key.toUpperCase -> o.   get(key).unwrapped().toString

      }
    }.toMap

    //val explainFramePath = config.getString("sru.explain")
    //val explainFrameContent = new String(env.classLoader().getResourceAsStream(explainFramePath).readAllBytes())

    val explainFrames = config.getObjectList("sru.explain").asScala.toSeq.map {
      o => {
        val key = o.keySet.asScala.head
        val path = o.get(key).unwrapped().toString
        key.toUpperCase -> new String(env.classLoader().getResourceAsStream(path).readAllBytes())

      }
    }.toMap

    val sruJavaConfig = new SruJavaConfig()
    sruJavaConfig.addViewPoints(mappingViewFilter.asJava)
    sruJavaConfig.addExplainFrames(explainFrames.asJava)

    config.getObjectList("sru.cqlrelations").asScala.toSeq.foreach {
      o => {
        val key = o.keySet.asScala.head
        val indexConfigs = o.get(key).asInstanceOf[ConfigObject]
        sruJavaConfig.addCqlRelationsMapping(new  MappingConfigValues(indexConfigs.get("supportedRelations").unwrapped().toString,
            indexConfigs.get("defaultRelation").unwrapped().toString,
            indexConfigs.get("alternativeIndex").unwrapped().toString,
            key.toLowerCase,
            new util.ArrayList[String](indexConfigs.get("solrFields").
              asInstanceOf[ConfigList].unwrapped().asScala.map(_.toString).asJava)
          ))

      }
    }

    val definedSchemas = config.getObjectList("sru.recordSchemata").asScala.toSeq.map((o:ConfigObject) => {
      val schemaName = o.keySet().asScala.toSeq.head
      val defaultValue = o.get(schemaName).asInstanceOf[ConfigObject].get("default").unwrapped().asInstanceOf[java.lang.Boolean]
      val sortValue = o.get(schemaName).asInstanceOf[ConfigObject].get("sort").unwrapped().asInstanceOf[String]
      (schemaName.toUpperCase -> new SruJavaConfig.SRUSchemaParameter(defaultValue,sortValue))
      }
    ).toMap

    sruJavaConfig.addSruSchemaMapping(definedSchemas.asJava)


    sruJavaConfig
  }

  initXSLTResponseTemplates()

  private def initXSLTResponseTemplates(): Unit = {

    val schema2Transformer = config.getObjectList("sru.recordSchemata").asScala.toSeq.map((o:ConfigObject) => {
      val schemaName = o.keySet().asScala.toSeq.head
      val resourcePath = o.get(schemaName).asInstanceOf[ConfigObject].get("xslt").unwrapped().toString
      if (resourcePath == "NONE") {
        schemaName -> SruSchema(schemaName,Option.empty[Transformer])
      } else {
        val transformer: Transformer = new TemplateCreator(resourcePath).createTransformer()
        schemaName -> SruSchema(schemaName,Option(transformer))
      }
    }
    ).toMap

    XSLTTemplateWrapper.init(schema2Transformer)

  }



  private def getSingleKeyObjects(path: String) = {
    config.getObjectList(path).asScala.toSeq.map {
      o => {
        val key = o.keySet.asScala.head
        key -> o.get(key).unwrapped().toString

      }
    }.toMap
  }



  override def explain(databaseEndpoint:String): Try[String] = Try {

    sruConfig.getExplainFrame(databaseEndpoint)

  }

  override def listLength: Int = ???


  override def searchRetrieve(query: String,
                              startRecord: Int,
                              maximumRecords: Int,
                              recordSchema: String,
                              dataBaseEndpointFilter: Option[String]
                             ): Try[ResultList] = Try {

    val queryParams = new QueryParameter(query,
      startRecord,
      maximumRecords,
      Optional.empty[String], //todo: implement cursor technic
      solrCollection)

    val solrQueryTransformation = new SOLRQueryTransformation

    //is used for subdbs
    solrQueryTransformation.setGeneralFilterQuery(dataBaseEndpointFilter.orNull)
    solrQueryTransformation.setCurrentSchema(recordSchema)


    //not ready
    solrQueryTransformation.init(
      queryParams,
      client.get, //todo avoid casting - how to handle change to Zclient
      sruConfig)


      val response: QueryResponse = solrQueryTransformation.runQuery()
      val results = response.getResults.asScala
      val fullrecordHoldingsList: mutable.Seq[SruContent] = results.map(doc => {

      val xmlFullRecord = XML.loadString(doc.getFieldValue("fullrecord").toString)
      val xmlHoldings = XML.loadString(doc.getFieldValue("holdings").toString)
      SruContent(xmlFullRecord, xmlHoldings)
    })

    ResultList(startRecord + maximumRecords, response.getResults.getNumFound.toInt, fullrecordHoldingsList.toSeq )


  }
}
