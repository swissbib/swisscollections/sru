import sbt.Keys._
import Dependencies._


ThisBuild / scalaVersion := "2.13.15"
ThisBuild / organization := "ch.swisscollections"
ThisBuild / organizationName := "swisscollections"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") Some(tag)
  else None
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning, PlayService, PlayLayoutPlugin)
  .settings(
    name := "sru",
    //assemblyJarName in assembly := "app.jar",
    //test in assembly := {},
    //mainClass in assembly := Some("Main"),
    resolvers ++= Seq(
      //"Memobase Utils" at "https://dl.bintray.com/jonas-waeber/memobase"
      "Typesafe" at "https://repo.typesafe.com/typesafe/releases/",
      "Restlet Repository" at "https://maven.restlet.talend.com",
      //"cql repository" at "https://maven.ceon.pl/artifactory/repo/"
      "cql repository" at "https://maven.indexdata.com/"

      //"Memobase Utils" at "https://dl.bintray.com/memoriav/memobase"
    ),

    assembly / assemblyJarName := "app.jar",
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case other if other.contains("module-info.class") => MergeStrategy.discard
      case "org/joda/time/tz/data/Pacific/*" => MergeStrategy.first
      //case x =>
      //  val oldStrategy = (assemblyMergeStrategy in assembly).value
      //  oldStrategy(x)
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case x =>  MergeStrategy.first
    },


    libraryDependencies ++= Seq(
      guice,
      joda,
      //lemonlabs,
      //codingwell,
      scalatestplusplay % Test,
      logging,
      //scala_xml_module,
      //upickl,
      solrj,
      //gson,
      //solr_analysis,
      ws,
      cql,
      marc4j
      //restlet_velocity
      //memobaseServiceUtils,
    ),
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    )

  )

PlayKeys.devSettings := Seq("play.server.http.port" -> "9000")

import com.typesafe.sbt.packager.MappingsHelper._
Universal / mappings ++= directory(baseDirectory.value / "resources")

