<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="xml"
                encoding="UTF-8"
                indent="yes"
                omit-xml-declaration="yes"/>

    <!--<xsl:param name="position" select="''"/>-->

	<xsl:template match="record">

        <record>

        <recordData>

            <record xmlns="http://www.loc.gov/MARC21/slim">
                <xsl:copy-of select="/record/*"/>
            </record>

        </recordData>

        </record>

	</xsl:template>
</xsl:stylesheet>