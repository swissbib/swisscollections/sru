# Sru API for swisscollections

This is the code for the SRU API for swisscollections.

Documenation : https://ub-basel.atlassian.net/wiki/spaces/RH/pages/1907130369/SRU

## Run locally

sbt "run 9001" -DRESPONSE_LISTLENGTH=10 -DSOLR_COLLECTION=swisscollectionstest -DSOLR_HOST=https://solrtest.swisscollections.unibas.ch/solr

And go to http://localhost:9001/swisscollections?version=1.2&operation=searchRetrieve&query=dc.anywhere%20ALL%20%22wetter%20motiv%22&recordSchema=ao-isad






