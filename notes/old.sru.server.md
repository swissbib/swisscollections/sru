
## bootstrapping properties alter server


```bash

#!/bin/sh

SRU_PROP_BASE=/var/lib/tomcat9
SRU_PROP_RES=${SRU_PROP_BASE}/webapps/sru/WEB-INF/classes/resources


CATALINA_OPTS="$CATALINA_OPTS
-Dmarc2DublinCoreTemplate=${SRU_PROP_RES}/xslt/MARC21slim2OAIDC.swissbib.xsl
-Dmarc2DublinCoreTemplateOCLC=${SRU_PROP_RES}/xslt/MARC21slim2OAIDC.oclc.xsl
-DdiagnoseDir=file://${SRU_PROP_RES}/diagnose
-DsolrServer=http://sgreenc2.swissbib.ch/solr/green###defaultdb####http://sbbc2.swissbib.ch/solr/bb###bbdb
-Dmarc2aoisadxml=${SRU_PROP_RES}/xslt/MARC21slim2aoisadxml.xsl
-DmappingFieldsProps=${SRU_PROP_RES}/mapping/mapping.solr.properties
-DmappingCQLRelations=${SRU_PROP_RES}/mapping/mapping.cqlrelations.properties
-DformResource=${SRU_PROP_RES}/diagnose/index.html
-DjsResource=${SRU_PROP_RES}/diagnose/js/srudiagnose.js
-DsruSearchURL=http://sru.swissbib.ch/sru/search
-DsruExplainURL=http://sru.swissbib.ch/sru/explain
-DxsltDir=${SRU_PROP_RES}/xslt/
-DsruExplain=${SRU_PROP_RES}/explain/explain.swissbib.default.xml
-DfilterDBs=${SRU_PROP_RES}/mapping/mapping.views.properties
-DlastCommit=https://github.com/swissbib/sruServer/releases/tag/1.2.2
-DmaxNumberDocuments=50000"

```